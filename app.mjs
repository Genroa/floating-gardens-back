import { createServer } from "http";
import { Server } from "socket.io";
import { PORT, initializeConfig } from "./config.mjs";
import { MONGO_DATABASE, MONGO_HOSTS } from "./config.mjs";
import {
  dataBackend,
  engine,
  setEngine,
  setDataBackend,
  setEngineEventsListener,
} from "./engine.js";
import { useMongoDBDataBackend } from "./backend_adapters/index.js";
import pkg, { EVENTS } from "@floating-gardens/engine";
import { setAuthListeners } from "./auth.js";
const { FloatingGardensEngine } = pkg;

async function main() {
  initializeConfig();

  // Network setup
  const httpServer = createServer();
  const io = new Server(httpServer, {
    cors: {
      origin: "*", //"http://localhost:9000",
    },
  });

  // Engine startup
  setDataBackend(
    await useMongoDBDataBackend({
      url: MONGO_HOSTS,
      dbName: MONGO_DATABASE,
    })
  );
  setEngine(new FloatingGardensEngine(dataBackend));
  await engine.loadData();
  if (Object.keys(engine.users).length === 0) {
    await engine.createNewUser({
      login: "test",
      password: "test",
      pseudo: "New Online User",
    });
  }

  // AUTH
  setAuthListeners(io);
  setEngineEventsListener(io);

  io.listen(PORT);
  engine.startUpdating();

  const exit = async () => {
    console.info("Exit signal received.");
    engine.setEventListener(EVENTS.STOPPED_SERVER, () => {
      console.log("Engine stopped. Let's exit the process");
      process.exit(0);
    });
    engine.stop();
  };
  process.on("SIGTERM", exit);
  process.on("SIGINT", exit);
}

main();
