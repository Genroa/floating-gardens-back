import jwt from "jsonwebtoken";
import { engine } from "./engine.js";
import {
  assignUserAndRenewToken,
  getUserByLogin,
  getUserByPassword,
} from "./services/users.js";

export function setAuthListeners(socketServer) {
  socketServer.on("connection", (socket) => {
    socket.authenticationTimeoutId = setTimeout(
      () => socket.disconnect(),
      5000
    );
    socket.on("authenticate", async (username, password, cb) => {
      // Check for user/password
      const user = await getUserByPassword(username, password);
      if (!user) {
        cb(null);
        socket.disconnect();
        return;
      }
      const party = engine.parties[user.party];
      assignUserAndRenewToken({ socket, user, party, cb });
    });

    socket.on("authenticate_token", async (token, cb) => {
      // Check token's validity
      try {
        const data = jwt.verify(token, "secret");
        // No password check, the token comes from us
        const user = await getUserByLogin(data.login);
        if (!user) {
          cb(null);
          socket.disconnect();
          return;
        }
        const party = engine.parties[user.party];
        assignUserAndRenewToken({ socket, user, party, cb });
      } catch (err) {
        cb(null);
        socket.disconnect();
        return;
      }
    });
  });
}
