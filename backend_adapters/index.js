import jwt from "jsonwebtoken";
import crypto from "crypto";
import { ObjectId } from "mongodb";
import { getDb } from "../db/db.mjs";

const findAll = async (coll) => {
  const cursor = await coll.find();
  const arr = await cursor.toArray();
  return Object.fromEntries(arr.map((el) => [el._id, el]));
};

export const useMongoDBDataBackend = async ({
  url,
  dbName,
  username,
  password,
}) => {
  const { usersCollection, partiesCollection, roomsCollection } = await getDb({
    url,
    dbName,
    username,
    password,
  });

  async function insertUser({
    login,
    password,
    pseudo,
    party,
    visitingRoom = null,
  }) {
    const salt = crypto.randomBytes(16).toString("hex");
    const hashedPassword = crypto
      .pbkdf2Sync(password, salt, 310000, 32, "sha256")
      .toString("hex");

    const { insertedId } = await usersCollection.insertOne({
      login,
      password: hashedPassword,
      salt,
      pseudo,
      party,
      lastActive: Date.now(),
      visitingRoom,
      currentAction: null,
    });
    return insertedId;
  }

  // RETURN
  return {
    users: usersCollection,
    parties: partiesCollection,
    rooms: roomsCollection,

    async loadData() {
      return {
        users: await findAll(usersCollection),
        parties: await findAll(partiesCollection),
        rooms: await findAll(roomsCollection),
      };
    },

    async saveAll({ users, parties, rooms }) {
      console.log("Saving...");
      for (const user of Object.values(users)) {
        await this.users.replaceOne({ _id: user._id }, user);
      }
      for (const party of Object.values(parties)) {
        await this.parties.replaceOne({ _id: party._id }, party);
      }
      for (const room of Object.values(rooms)) {
        await this.rooms.replaceOne({ _id: room._id }, room);
      }
      console.log("Saving done.");
    },

    async createNewUser({ login, password, pseudo, partyId }) {
      let partyIdToUse = partyId;
      let roomIdToStartIn = null;
      if (!partyIdToUse) {
        // Create a new room
        const { insertedId: roomId } = await this.rooms.insertOne({
          name: "New room",
          biome: "VINE_RUIN",
          elements: [],
          state: {},
        });
        roomIdToStartIn = roomId;
        const { insertedId: inventoryId } = await this.rooms.insertOne({
          name: "Inventory",
          biome: "",
          elements: [],
          state: {},
        });

        // Create new party
        const { insertedId } = await this.parties.insertOne({
          rooms: [String(roomId)],
          inventory: String(inventoryId),
        });
        partyIdToUse = insertedId;
      }
      // if a partyId was provided, the room you arrive in is the first one of the party
      else {
        let party = await this.parties.findOne({ _id: ObjectId(partyId) });
        roomIdToStartIn = party.rooms[0];
      }

      const { insertedId: userId } = await insertUser({
        login,
        password,
        pseudo,
        party: String(partyIdToUse),
        visitingRoom: String(roomIdToStartIn),
        currentAction: null,
      });

      return {
        user: await this.users.findOne({ _id: ObjectId(userId) }),
        party: await this.parties.findOne({ _id: ObjectId(partyId) }),
        room: await this.rooms.findOne({ _id: ObjectId(roomId) }),
      };
    },
  };
};
