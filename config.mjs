// "mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTS}/${MONGO_DATABASE}?authSource=${MONGO_DATABASE}&appname=${MONGO_DATABASE}&w=majority&tls=true&tlscafile=${MONGO_CA_PATH}&replicaSet=${MONGO_REPLICASET}"
// DB CONNECTION CONFIG
export let MONGO_HOSTS = "0.0.0.0:27017";
export let MONGO_DATABASE = "floatinggardens";
export let MONGO_USERNAME = "";
export let MONGO_PASSWORD = "";
export let MONGO_CA_PATH = "";
export let MONGO_REPLICASET = "";
export let USE_TEST_DATA = false;

// WSS SERVER CONFIG
export let PORT = 3011;

export function initializeConfig() {
  MONGO_HOSTS = process.env.MONGO_HOSTS ?? MONGO_HOSTS;
  MONGO_DATABASE = process.env.MONGO_DATABASE ?? MONGO_DATABASE;
  MONGO_USERNAME = process.env.MONGO_USERNAME ?? MONGO_USERNAME;
  MONGO_PASSWORD = process.env.MONGO_PASSWORD ?? MONGO_PASSWORD;
  MONGO_CA_PATH = process.env.MONGO_CA_PATH ?? MONGO_CA_PATH;
  MONGO_REPLICASET = process.env.MONGO_REPLICASET ?? MONGO_REPLICASET;
  USE_TEST_DATA = process.env.USE_TEST_DATA ?? USE_TEST_DATA;

  PORT = process.env.PORT ?? PORT;
}
