import { MongoClient } from "mongodb";
import { runUpgradeScripts } from "./upgrade_script.js";

export let dbConnection = null;
export let connection = null;

export function buildMongoURL({ url, dbName, username, password }) {
  const credentials = username && password ? `${username}:${password}@` : "";
  return `mongodb://${credentials}${url}/${dbName}`;
}

export const getDb = async ({ url, dbName, username, password }) => {
  if (dbConnection) return dbConnection;
  const uri = buildMongoURL({ url, dbName, username, password });
  console.log("Connecting to the mongoDB URI", uri);
  connection = await MongoClient.connect(uri);

  dbConnection = connection.db(dbName);
  const usersCollection = dbConnection.collection("users");
  const partiesCollection = dbConnection.collection("parties");
  const roomsCollection = dbConnection.collection("rooms");

  await runUpgradeScripts({
    connection,
    dbConnection,
    usersCollection,
    partiesCollection,
    roomsCollection,
  });

  return {
    connection,
    dbConnection,
    usersCollection,
    partiesCollection,
    roomsCollection,
  };
};
