import { ACTIONS, cleanUser, EVENTS } from "@floating-gardens/engine";
import { getUserSocket } from "./services/sockets.js";

export let dataBackend = null;
export let engine = null;

export function setDataBackend(d) {
  dataBackend = d;
}
export function setEngine(e) {
  engine = e;
}

export function addSocketToRooms(socket) {
  const user = socket.user;
  const party = socket.party;

  socket.join(String(user._id));
  socket.join(String(party._id));
  socket.join(user.visitingRoom);
}

export function assignListenersToSocket(socket) {
  for (const actionId of Object.keys(ACTIONS)) {
    socket.on(`ACTION:${actionId}`, (data, cb) => {
      console.log(`Received action of type "${actionId}"`);
      engine.addActionToHandle({
        actionId,
        context: { engine, user: socket.user, party: socket.party },
        data,
        cb,
      });
    });
  }
}

export function setEngineEventsListener(socketServer) {
  // Update to a room
  // Notify: the visitors of the room
  engine.setEventListener(EVENTS.UPDATED_ROOM, (room) => {
    socketServer
      .to(String(room._id))
      .emit(`EVENT:${EVENTS.UPDATED_ROOM}`, room);
  });

  // Update to a user of your party
  // Notify: the party
  engine.setEventListener(EVENTS.UPDATED_USER, (user) => {
    socketServer.to(user.party).emit(`EVENT:${EVENTS.UPDATED_USER}`, user);
  });

  // Update to a party
  // Notify: the party
  engine.setEventListener(EVENTS.UPDATED_PARTY, (party) => {
    socketServer
      .to(String(party._id))
      .emit(`EVENT:${EVENTS.UPDATED_PARTY}`, party);
  });

  engine.setEventListener(EVENTS.USER_MOVED, ({ previousRoomId, user }) => {
    const userSocket = getUserSocket(socketServer, user);
    userSocket.leave(previousRoomId);
    userSocket.join(user.visitingRoom);
    socketServer.to(String(user.party)).emit(`EVENT:${EVENTS.USER_MOVED}`, {
      previousRoomId,
      user,
    });
  });
}
