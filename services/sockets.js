export function getUserSocket(socketServer, user) {
  return [...socketServer.sockets.sockets.values()].find(
    (s) => s.user._id === user._id
  );
}
