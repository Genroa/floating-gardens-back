import jwt from "jsonwebtoken";
import crypto from "crypto";
import {
  addSocketToRooms,
  assignListenersToSocket,
  engine,
} from "../engine.js";
import { cleanUser } from "@floating-gardens/engine";

export async function getUserByPassword(login, password) {
  const user = await getUserByLogin(login);
  if (!user) return null;

  const hashedPassword = crypto.pbkdf2Sync(
    password,
    user.salt,
    310000,
    32,
    "sha256"
  );
  if (
    !crypto.timingSafeEqual(Buffer.from(user.password, "hex"), hashedPassword)
  ) {
    return null;
  }
  return user;
}

export async function getUserByLogin(login) {
  return Object.values(engine.users).find((u) => u.login === login);
}

export async function getUserById(id) {
  return engine.users[id];
}

export function assignUserAndRenewToken({ socket, user, party, cb }) {
  socket.user = user;
  socket.party = party;
  const newToken = jwt.sign({ login: user.login }, "secret", {
    expiresIn: "1h",
  });
  addSocketToRooms(socket);
  assignListenersToSocket(socket);
  cb({
    token: newToken,
    user: cleanUser(user),
    party: party,
  });
  clearTimeout(socket.authenticationTimeoutId);
}
