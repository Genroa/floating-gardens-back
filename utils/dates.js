export function getPeriodsForYear(pollingRate, year, entireYear = false) {
  return getAllPeriodsGoingBackYears({
    pollingRate,
    startYear: year,
    endYear: year,
    entireYear,
  });
}

export function getAllPeriodsGoingBackYears({
  pollingRate,
  howManyYears,
  startYear,
  endYear,
  entireYear = false,
}) {
  const now = new Date();
  const end = entireYear
    ? Date.UTC((endYear ?? new Date().getFullYear()) + 1)
    : Date.now();
  const beginningOldestPeriod = new Date(
    Date.UTC(startYear ?? now.getFullYear() - howManyYears)
  );
  const periods = [];
  let currentPeriod = beginningOldestPeriod;
  // console.log("Start is", beginningOldestPeriod.toUTCString(), "end is", new Date(end).toUTCString())
  while (currentPeriod.getTime() < end) {
    periods.push(currentPeriod);

    if (currentPeriod.getMonth() + pollingRate > 11) {
      currentPeriod = new Date(Date.UTC(currentPeriod.getFullYear() + 1));
    } else {
      let newDate = new Date(
        Date.UTC(
          currentPeriod.getFullYear(),
          currentPeriod.getMonth() + pollingRate
        )
      );
      currentPeriod = newDate;
    }
  }
  return periods;
}